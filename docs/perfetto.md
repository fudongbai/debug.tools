---
title: perfetto
updated: 2024-05-04 22:07:01+0800
created: 2024-05-04 22:07:01+0800
weather: "Qingdao: ☀️   +16°C"
latitude: 36.0649
longitude: 120.3804
categories:
tags:
tags:
    - perfetto
    - trace
---


# Build perfetto
```sh
git clone https://github.com/google/perfetto -b v43.1
git clone https://android.googlesource.com/platform/external/perfetto/ && cd perfetto
git checkout v43.1
```


=== "arm64"
    ```sh
    # tools/gn clean out/linux
    tools/install-build-deps --linux-arm
    tools/gn gen out/linux

    printf '%s\n' > out/linux/args.gn \
        'is_debug=false' \
        'is_system_compiler=true' \
        'is_hermetic_clang=false' \
        'is_clang=false' \
        'target_cpu="arm64"' \
        'target_os="linux"' \
        'target_cc="aarch64-poky-linux-gcc"' \
        'target_ar="aarch64-poky-linux-ar"' \
        'target_cxx="aarch64-poky-linux-g++"' \
        'target_strip="aarch64-poky-linux-strip"' \
        'target_sysroot="/opt/poky/4.0.15/sysroots/cortexa53-crypto-poky-linux/"' \
        'extra_host_cflags="-Wno-error"'
    tools/ninja -C out/linux tracebox traced traced_probes perfetto 
    ```


=== "arm"
    ```sh
    tools/install-build-deps --linux-arm
    tools/gn gen out/linux

    printf '%s\n' > out/linux/args.gn \
        'is_debug=false' \
        'is_system_compiler=true' \
        'is_hermetic_clang=false' \
        'is_clang=false' \
        'target_cpu="arm"' \
        'target_os="linux"' \
        'target_cc="arm-poky-linux-gnueabi-gcc"' \
        'target_ar="arm-poky-linux-gnueabi-ar"' \
        'target_cxx="arm-poky-linux-gnueabi-g++"' \
        'target_strip="arm-poky-linux-gnueabi-strip"' \
        'target_sysroot="/opt/poky/4.0.15/sysroots/cortexa7t2hf-neon-vfpv4-poky-linux-gnueabi"' \
        'extra_target_cflags="-mfpu=neon-vfpv4 -mfloat-abi=hard"' \
        'extra_target_ldflags="-Wl,-dynamic-linker,/lib/ld-linux-armhf.so.3"' \
        'extra_host_cflags="-Wno-error"'
    tools/ninja -C out/linux tracebox traced traced_probes perfetto
    ```

    ```sh
    file out/linux/traced
    out/linux/traced: ELF 32-bit LSB pie executable, ARM, EABI5 version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux-armhf.so.3, for GNU/Linux 3.2.0, BuildID[sha1]=7e0f496cbc9bb1adcac819b94469c13d00b56a9d, with debug_info, not stripped
    scp out/linux/stripped/libperfetto.so root@192.168.1.80:/usr/lib/
    ```


# Capture trace
```sh
scp out/linux/tracebox root@192.168.1.81:/tmp/
scp test/configs/scheduling.cfg root@192.168.1.81:/tmp/
```

```sh
tracebox -o trace_file.perfetto-trace --txt -c scheduling.cfg
```



# Visualization

Head to [https://ui.perfetto.dev][ui], and open the trace file with it,  or build
local ui server:
```sh
ui/run-dev-server --out out/ui
[...]
First build completed!
Starting HTTP server on http://127.0.0.1:10000
[33.873] 68/68	makeManifest
```



# convert
```sh
curl -L https://get.perfetto.dev/traceconv -o $HOME/.local/bin/traceconv
chmod +x $HOME/.local/bin/traceconv
traceconv [text|json|systrace|profile] [input proto file] [output file]

traceconv text /tmp/trace_file.perfetto-trace perfetto.txt
```



[perfetto]: https://perfetto.dev
[github]: https://github.com/google/perfetto
[arm64]: https://perfetto.dev/docs/contributing/build-instructions#cross-compiling-for-linux-arm-64
[gist]: https://gist.github.com/gh2o/d116832fe31bb5be09fab5b171082e7b
[config]: https://perfetto.dev/docs/reference/trace-config-proto
[linux-tracing]:https://perfetto.dev/docs/quickstart/linux-tracing
[ui]: https://ui.perfetto.dev
[traceconv]: https://perfetto.dev/docs/quickstart/traceconv
