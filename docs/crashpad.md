---
title: crashpad
updated: 2024-05-04 22:05:10+0800
created: 2024-05-04 22:05:10+0800
weather: "Qingdao: ☀️   +16°C"
latitude: 36.0649
longitude: 120.3804
categories:
  - crash
tags:
  - minidump
---

##

# Fetch code

```sh
git clone https://github.com/getsentry/crashpad.git
git checkout 96e301 -b build
git submodule update --init --recursive
```



# patches for mips

```diff
From 4a3cffd9611778b6578b1950b0ac2d72c16eae6d Mon Sep 17 00:00:00 2001
From: Fudong <fudongbai@gmail.com>
Date: Tue, 14 May 2024 05:52:04 +0800
Subject: [PATCH] Fix build failure for mips

Signed-off-by: Fudong <fudongbai@gmail.com>
---
 snapshot/linux/exception_snapshot_linux.cc | 1 +
 snapshot/linux/thread_snapshot_linux.cc    | 1 +
 util/net/http_transport_libcurl.cc         | 2 ++
 3 files changed, 4 insertions(+)

diff --git a/snapshot/linux/exception_snapshot_linux.cc b/snapshot/linux/exception_snapshot_linux.cc
index 677afdaa..207807ed 100644
--- a/snapshot/linux/exception_snapshot_linux.cc
+++ b/snapshot/linux/exception_snapshot_linux.cc
@@ -12,6 +12,7 @@
 // See the License for the specific language governing permissions and
 // limitations under the License.

+#include <string.h>
 #include "snapshot/linux/exception_snapshot_linux.h"

 #include <signal.h>
diff --git a/snapshot/linux/thread_snapshot_linux.cc b/snapshot/linux/thread_snapshot_linux.cc
index 6489387f..8f7bc12d 100644
--- a/snapshot/linux/thread_snapshot_linux.cc
+++ b/snapshot/linux/thread_snapshot_linux.cc
@@ -12,6 +12,7 @@
 // See the License for the specific language governing permissions and
 // limitations under the License.

+#include <string.h>
 #include "snapshot/linux/thread_snapshot_linux.h"

 #include <sched.h>
diff --git a/util/net/http_transport_libcurl.cc b/util/net/http_transport_libcurl.cc
index ba31d6a9..a2e03712 100644
--- a/util/net/http_transport_libcurl.cc
+++ b/util/net/http_transport_libcurl.cc
@@ -239,6 +239,8 @@ std::string UserAgent() {
 #endif
 #elif defined (ARCH_CPU_RISCV64)
     static constexpr char arch[] = "riscv64";
+#elif defined (ARCH_CPU_MIPSEL)
+    static constexpr char arch[] = "mips";
 #else
 #error Port
 #endif
--
2.34.1
```


# Dependencies

## zlib
```sh
wget -c https://www.zlib.net/zlib-1.3.1.tar.xz
```

=== "mips"
    ```sh
    export PATH=/usr/local/mips/mipsel-buildroot-linux-gnu/bin:$PATH
    CHOST=mipsel-linux ./configure --prefix=/usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr
    make && make install

    # or build with cmake
    cmake -B build -DCMAKE_C_COMPILER=mipsel-linux-gcc \
        -DCMAKE_INSTALL_PREFIX=/usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr
    cmake --build build --parallel
    cmake --install build
    ```

=== "arm"
    ```sh
    export PATH=/usr/local/arm/arm-buildroot-linux-gnueabihf/bin/:$PATH
    CHOST=arm-linux ./configure --prefix=/usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr
    make && make install

    # or build with cmake
    cmake -B build -DCMAKE_C_COMPILER=arm-linux-gcc \
        -DCMAKE_INSTALL_PREFIX=/usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr/
    cmake --build build --parallel
    cmake --install build
    ```

=== "arm64"
    ```sh
    export PATH=/usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/bin:$PATH
    CHOST=aarch64-linux-gnu ./configure --prefix=/usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr
    make && make install

    # or build with cmake
    cmake -B build -DCMAKE_C_COMPILER=aarch64-linux-gnu-gcc \
        -DCMAKE_INSTALL_PREFIX=/usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/
    cmake --build build --parallel
    cmake --install build
    ```


## curl

```sh
wget -c https://curl.se/download/curl-8.7.1.tar.xz
```

=== "mips"
    ```sh
    ./configure --host=mipsel-linux \
		--prefix=/usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr \
		--disable-manual \
		--disable-ntlm-wb \
		--with-random=/dev/urandom \
		--disable-curldebug \
		--disable-libcurl-option \
		--disable-ldap \
		--disable-ldaps \
		--enable-threaded-resolver \
		--without-ssl \
		--without-libpsl
    make && make install

    # build with cmake
    cmake -B build \
        -DCMAKE_C_COMPILER=mipsel-linux-gcc \
        -DCURL_ENABLE_SSL=OFF \
        -DCMAKE_INSTALL_PREFIX=/usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr
    cmake --build build --parallel
    cmake --install build
    ```

=== "arm"
    ```sh
    ./configure --host=arm-linux \
		--prefix=/usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr \
		--disable-manual \
		--disable-ntlm-wb \
		--with-random=/dev/urandom \
		--disable-curldebug \
		--disable-libcurl-option \
		--disable-ldap \
		--disable-ldaps \
		--enable-threaded-resolver \
		--without-ssl \
		--without-libpsl
    make && make install

    # build with cmake
    cmake -B build \
        -DCMAKE_C_COMPILER=arm-linux-gcc \
        -DCURL_ENABLE_SSL=OFF \
        -DCMAKE_INSTALL_PREFIX=/usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr/
    cmake --build build --parallel
    cmake --install build
    ```

=== "arm64"
    ```sh
    ./configure --host=aarch64-linux-gnu \
		--prefix=/usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr \
		--disable-manual \
		--disable-ntlm-wb \
		--with-random=/dev/urandom \
		--disable-curldebug \
		--disable-libcurl-option \
		--disable-ldap \
		--disable-ldaps \
		--enable-threaded-resolver \
		--without-ssl \
		--without-libpsl
    make && make install

    # build with cmake
    cmake -B build \
        -DCMAKE_C_COMPILER=aarch64-linux-gnu-gcc \
        -DCURL_ENABLE_SSL=OFF \
        -DCMAKE_INSTALL_PREFIX=/usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/
    cmake --build build --parallel
    cmake --install build
    ```



# Cross Compile

=== "mips"
    ```sh
    cmake -B build -DCMAKE_C_COMPILER=mipsel-linux-gcc
    cmake --build build --parallel
    cmake --install build --prefix /usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr

    sed -i '/LINK_ONLY:CURL::libcurl/i \
      INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/include/crashpad"'\
        /usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr/lib/cmake/crashpad/crashpad-targets.cmake

    scp build/handler/crashpad_handler root@192.168.1.91:/bin/
    ```

=== "arm"
    ```sh
    cmake -B build -DCMAKE_C_COMPILER=arm-linux-gcc
    cmake --build build --parallel
    cmake --install build --prefix /usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr/

    sed -i '/LINK_ONLY:CURL::libcurl/i \
      INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/include/crashpad"'\
      /usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr/lib/cmake/crashpad/crashpad-targets.cmake

    scp build/handler/crashpad_handler root@192.168.1.80:/bin/
    ```

=== "arm64"
    ```sh
    cmake -B build -DCMAKE_C_COMPILER=aarch64-linux-gnu-gcc
    cmake --build build --parallel
    cmake --install build --prefix /usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr

    sed -i '/LINK_ONLY:CURL::libcurl/i \
      INTERFACE_INCLUDE_DIRECTORIES "${_IMPORT_PREFIX}/include/crashpad"'\
      /usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/lib/cmake/crashpad/crashpad-targets.cmake

    scp build/handler/crashpad_handler root@192.168.1.81:/bin/
    ```



# Example

```cpp title="crashme.cpp" linenums="1"
#include <stdio.h>
#include <atomic>
#include <chrono>
#include <iostream>
#include <map>
#include <string>
#include <thread>
#include <vector>

#include "client/crash_report_database.h"
#include "client/crashpad_client.h"
#include "client/crashpad_info.h"
#include "client/settings.h"

using namespace crashpad;

int init_crashpad()
{
	// Cache directory that will store crashpad information and minidumps
	base::FilePath database("crashpad.db");
	// Path to the out-of-process handler executable
	base::FilePath handler("/bin/crashpad_handler");
	// URL used to submit minidumps to
	std::string url(
			"http://192.168.1.78:9000/api/9/minidump/"
			"?sentry_client=crashpad/0.8.9&sentry_key=33ebe98b8197ef62694a73c2b38aa7f0");
	// Optional annotations passed via --annotations to the handler
	std::map<std::string, std::string> annotations;
	// Optional arguments to pass to the handler
	std::vector<std::string> arguments;

	arguments.push_back("--no-rate-limit");

	std::vector<base::FilePath> attachments;
	attachments.push_back(base::FilePath("/etc/fstab"));

	CrashpadClient client;
	bool success = client.StartHandler(handler,
			database,
			base::FilePath(),
			url,
			"",
			std::map<std::string, std::string>(),
			std::vector<std::string>(),
			/* restartable */ true,
			/* asynchronous_start */ false,
			attachments);

	if (success) {
		printf("Started client handler.\n");
	} else {
		printf("Failed to start client handler.\n");
	}

	if (!success) {
		return 1;
	}

	std::unique_ptr<CrashReportDatabase> db =
		CrashReportDatabase::Initialize(database);

	if (db != nullptr && db->GetSettings() != nullptr) {
		db->GetSettings()->SetUploadsEnabled(true);
	}

	// Ensure that the simple annotations dictionary is set in the client.
	CrashpadInfo* crashpad_info = CrashpadInfo::GetCrashpadInfo();

	return 0;
}

void crash()
{
	volatile int* a = (int*)(NULL);
	*a = 1;
}

int main(int args, char* argv[])
{
	init_crashpad();

	crash();
}
```

``` cmake title="CMakeLists.txt" linenums="1"
cmake_minimum_required(VERSION 3.5)

project(crashme)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -g")

find_package(crashpad REQUIRED)
find_package(CURL REQUIRED)

add_executable(crashme crashme.cpp)

# Add BuildID for arm
target_link_options(crashme PUBLIC "-Wl,--build-id")
target_link_libraries(crashme crashpad::client crashpad::util crashpad::mini_chromium)
```

```sh linenums="1" title="stack_decode.sh"
--8<-- "docs/assets/scripts/stack_decode.sh"
```


=== "mips"
    ```sh
    cmake -B build -DCMAKE_C_COMPILER=mipsel-linux-gcc
    cmake --build build --parallel
    ssh root@192.168.1.91 mkdir /tmp/crashpad
    scp stack_decode.sh root@192.168.1.91:/tmp/crashpad/
    scp build/crashme root@192.168.1.91:/tmp/crashpad/
    ```

=== "arm"
    ```sh
    cmake -B build -DCMAKE_C_COMPILER=arm-linux-gcc
    cmake --build build --parallel
    ssh root@192.168.1.80 mkdir /tmp/crashpad
    scp stack_decode.sh root@192.168.1.80:/tmp/crashpad/
    scp build/crashme root@192.168.1.80:/tmp/crashpad/
    ```

=== "arm64"
    ```sh
    cmake -B build -DCMAKE_C_COMPILER=aarch64-linux-gnu-gcc
    cmake --build build --parallel
    ssh root@192.168.1.81 mkdir /tmp/crashpad
    scp stack_decode.sh root@192.168.1.81:/tmp/crashpad/
    scp build/crashme root@192.168.1.81:/tmp/crashpad/
    ```



# Stack unwinding

=== "mips"
    ```
    --8<-- "docs/assets/crashpad/crashpad-crashme-mips-stack-unwind.txt"
    ```
=== "arm"
    ```
    --8<-- "docs/assets/crashpad/crashpad-crashme-arm-stack-unwind.txt"
    ```
=== "arm64"
    ```
    --8<-- "docs/assets/crashpad/crashpad-crashme-arm64-stack-unwind.txt"
    ```



# Upload minidump to sentry

## Stack unwinding with minidump symbol

=== "mips"
    ```sh
    dump_syms build/crashme > crashme.sym
    sentry-cli debug-files upload --wait crashme.sym
    ```
    ![](assets/crashpad/debug-files-sym-mips.png)
    ![](assets/crashpad/crashpad-crashme-mips-sym.png)

=== "arm"
    ```sh
    dump_syms build/crashme > crashme.sym
    sentry-cli debug-files upload --wait crashme.sym
    > Found 1 debug information file
    > Prepared debug information file for upload
    > Uploading completed in 0.225s
    > Uploaded 1 missing debug information file
    > File processing complete:

           OK 56630eb9-7220-eccb-96b9-95cdc5fea1db (crashme; arm debug companion)
    ```
    ![](assets/crashpad/debug-files-sym-arm.png)
    ![](assets/crashpad/crashpad-crashme-arm-sym.png)

=== "arm64"
    ```sh
    dump_syms build/crashme > crashme.sym
    sentry-cli debug-files upload --wait crashme.sym
    ```
    ![](assets/crashpad/debug-files-sym-arm64.png)
    ![](assets/crashpad/crashpad-crashme-arm64-sym.png)


## Stack unwinding with ELF binary
=== "mips"
    ```sh
    sentry-cli debug-files upload --wait build/crashme
    ```
    ![](assets/crashpad/crashpad-debug-files-elf-mips.png)
    ```
    /tmp/crashpad # ./crashme
    Started client handler.
    Prepare to crash, sleeping for 1 second(s)
    [3019:3019:20240518,123609.343175:ERROR process_memory_linux.cc:50] pread64: Input/output error (5)
    [3019:3019:20240518,123609.347075:FATAL cpu_context.cc:195] Check failed: false.
    Segmentation fault
    ```
    ![](assets/crashpad/crashpad-crashme-mips-elf.png)

=== "arm"
    ```sh
    sentry-cli debug-files upload --wait build/crashme
    > Found 1 debug information file
    > Prepared debug information file for upload
    > Uploading completed in 0.221s
    > Uploaded 1 missing debug information file
    > File processing complete:

           OK 8329fcc7-4c6f-926f-a09b-8153fe4887a2 (crashme; arm executable)
    ```
    ![](assets/crashpad/crashpad-debug-files-elf-arm.png)
    ```sh
    root@raspberrypi3:/var/volatile/tmp/crashpad# ./crashme
    Started client handler.
    Segmentation fault
    ```
    ![](assets/crashpad/crashpad-crashme-arm-elf-build-id.png)

=== "arm64"
    ```sh
    sentry-cli debug-files upload --wait build/crashme
    > Found 1 debug information file
    > Prepared debug information file for upload
    > Uploading completed in 0.134s
    > Uploaded 1 missing debug information file
    > File processing complete:

           OK 852841da-5ee6-b38c-2a5b-afbef2386c2d (crashme; arm64 executable)
    ```
    ![](assets/crashpad/crashpad-debug-files-elf-arm64.png)
    ```sh
    root@roc-rk3328-cc:/var/volatile/tmp/crashpad# ./crashme
    Started client handler.
    Segmentation fault
    ```
    ![](assets/crashpad/crashpad-crashme-arm64-elf.png)


## Stack unwinding with source bundle
=== "mips"
    ```sh
    sentry-cli debug-files upload --include-sources build/crashme
    > Found 1 debug information file
    > Resolved source code for 1 debug information file
    > Prepared debug information files for upload
    > Uploading completed in 1.692s
    > Uploaded 2 missing debug information files
    > File upload complete:

      UPLOADED 1152c699-0bff-0559-faff-647e0468bf2f (crashme; mips sources)
      UPLOADED 1152c699-0bff-0559-faff-647e0468bf2f (crashme; mips executable)
    ```
    ![](assets/crashpad/crashpad-debug-files-source-bundle-mips.png)
    ```
    /tmp/crashpad # ./crashme
    Started client handler.
    Prepare to crash, sleeping for 1 second(s)
    [3019:3019:20240518,123609.343175:ERROR process_memory_linux.cc:50] pread64: Input/output error (5)
    [3019:3019:20240518,123609.347075:FATAL cpu_context.cc:195] Check failed: false.
    Segmentation fault
    ```
    ![](assets/crashpad/crashpad-crashme-source-bundle-mips.png)

=== "arm"
    ```sh
    sentry-cli debug-files upload --include-sources --wait build/crashme
    > Found 1 debug information file
    > Resolved source code for 1 debug information file
    > Prepared debug information files for upload
    > Uploading completed in 0.746s
    > Uploaded 2 missing debug information files
    > File processing complete:

           OK 3def0e09-6f77-8586-13ef-b047a828d572 (crashme; arm sources)
           OK 3def0e09-6f77-8586-13ef-b047a828d572 (crashme; arm executable)
    ```
    ![](assets/crashpad/crashpad-debug-files-source-bundle-arm.png)
    ![](assets/crashpad/crashpad-crashme-source-bundle-build-id-arm.png)

=== "arm64"
    ```sh
    sentry-cli debug-files upload --include-sources --wait build/crashme
    > Found 1 debug information file
    > Resolved source code for 1 debug information file
    > Prepared debug information files for upload
    > Uploading completed in 0.664s
    > Uploaded 2 missing debug information files
    > File processing complete:

           OK c20cca4d-6444-5acd-cbb8-8660605769a5 (crashme; arm64 executable)
           OK c20cca4d-6444-5acd-cbb8-8660605769a5 (crashme; arm64 sources)
    ```
    ![](assets/crashpad/crashpad-debug-files-source-bundle-arm64.png)
    ![](assets/crashpad/crashpad-crashme-source-bundle-arm64.png)



[crashpad]: https://chromium.googlesource.com/crashpad/crashpad
