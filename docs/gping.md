---
title: gping
updated: 2024-06-13 20:53:08+0800
created: 2024-06-13 20:53:08+0800
weather: "Qingdao: ☀️   +22°C"
latitude: 36.0649
longitude: 120.3804
categories:
    - network
tags:
---

# Install
```sh
cargo install gping
```


[gping]: https://github.com/orf/gping
