---
title: codechecker
updated: 2024-05-26 05:44:36+0800
created: 2024-05-26 05:44:36+0800
weather: "Qingdao: ⛅️  +18°C"
latitude: 36.0649
longitude: 120.3804
categories:
tags:
---

# Install CodeChecker and dependencies

```sh
pip3 install codechecker
sudo apt install -y cppcheck clang clang-tidy clang-tools
CodeChecker server --host 192.168.1.15
```



# Code Analysis
```sh
CodeChecker check -b make -o $(pwd)/results
CodeChecker store ./results -n helloworld --url http://192.168.1.15:8001/Default
```



[codechecker]: https://github.com/Ericsson/codechecker
