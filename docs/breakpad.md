---
title: breakpad
updated: 2024-05-04 22:04:56+0800
created: 2024-05-04 22:04:56+0800
weather: "Qingdao: ☀️   +16°C"
latitude: 36.0649
longitude: 120.3804
categories:
  - crash
tags:
  - minidump
---

##

# Fetch code

```sh
mkdir breakpad && cd breakpad
fetch breakpad
```


# Cross Compile

=== "mips"
    ```sh
    cd src
    export PATH=/usr/local/mips/mipsel-buildroot-linux-gnu/bin:$PATH
    sudo chown fdbai:fdbai -R /usr/local/mips/mipsel-buildroot-linux-gnu
    ./configure --host=mipsel-linux \
        --prefix=/usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr/
    make && make install
    ```

=== "arm"
    ```sh
    cd src
    export PATH=/usr/local/arm/arm-buildroot-linux-gnueabihf/bin/:$PATH
    ./configure --host=arm-linux \
        --prefix=/usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr/
    make && make install
    ```

=== "arm64"
    ```sh
    wget -c https://snapshots.linaro.org/gnu-toolchain/11.3-2022.06-1/aarch64-linux-gnu/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu.tar.xz
    cd src
    export PATH=/usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/bin:$PATH
    ./configure --host=aarch64-linux-gnu \
        --prefix=/usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr
    make && make install
    ```

```sh
mount -t overlayfs -o lowerdir=/bin,upperdir=/data/bin overlay /bin
scp src/tools/linux/dump_syms/dump_syms root@192.168.1.91:/bin/
scp src/processor/minidump_stackwalk root@192.168.1.91:/bin/ 
```


# Example

```c++  linenums="1" title="crashme.cpp"
#include <breakpad/client/linux/handler/exception_handler.h>

static bool dumpCallback(const google_breakpad::MinidumpDescriptor& descriptor,
		void* context, bool succeeded)
{
	printf("Dump path: %s\n", descriptor.path());
	return succeeded;
}

void crash()
{
	volatile int* a = (int*)(NULL);
	*a = 1;
}

int main(int argc, char* argv[])
{
	google_breakpad::MinidumpDescriptor descriptor("/tmp");
	google_breakpad::ExceptionHandler eh(descriptor, NULL, dumpCallback, NULL, true, -1);
	crash();
	return 0;
}
```

```Makefile  linenums="1" title="Makefile"
CFLAGS+=$(shell pkg-config --define-prefix --cflags breakpad-client)
LDFLAGS+=$(shell pkg-config --define-prefix --libs breakpad-client)

crashme: crashme.cpp
   	$(CPP) $(CFLAGS) -g $< -o $@ $(LDFLAGS)

clean:
	rm -f crashme
```

```sh linenums="1" title="stack_decode.sh"
--8<-- "docs/assets/scripts/stack_decode.sh"
```

=== "mips"
    ```sh
    CPP=mipsel-linux-g++ make
    ssh root@192.168.1.91 mkdir /tmp/breakpad
    scp crashme root@192.168.1.91:/tmp/breakpad
    scp stack_decode.sh root@192.168.1.91:/tmp/breakpad/
    ```

=== "arm"
    ```sh
    CPP=arm-linux-g++ make
    ssh root@192.168.1.80 mkdir /tmp/breakpad
    scp crashme root@192.168.1.80:/tmp/breakpad
    scp stack_decode.sh root@192.168.1.80:/tmp/breakpad/
    ```

=== "arm64"
    ```sh
    export PKG_CONFIG_PATH=/usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/lib/pkgconfig
    CPP=aarch64-linux-gnu-g++ make
    ssh root@192.168.1.81 mkdir /tmp/breakpad
    scp crashme root@192.168.1.81:/tmp/breakpad
    scp stack_decode.sh root@192.168.1.81:/tmp/breakpad/
    ```



# Stack unwinding

=== "mips"
    ```
    --8<-- "docs/assets/breakpad/breakpad-crashme-mips-stack-unwind.txt"
    ```
=== "arm"
    ```
    --8<-- "docs/assets/breakpad/breakpad-crashme-arm-stack-unwind.txt"
    ```
=== "arm64"
    ```
    --8<-- "docs/assets/breakpad/breakpad-crashme-arm64-stack-unwind.txt"
    ```


# Upload minidump to sentry
``` linenums="1" title="$HOME/.sentryclirc"
[defaults]
project=crashme
org=sentry

[auth]
dsn=http://b134d88980c1686c74a78d4a1c369a31@192.168.1.78:9000/7
token=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
```


## Stack unwinding with minidump symbol

=== "mips"
    ```sh
    dump_syms crashme > crashme.sym
    sentry-cli debug-files upload --wait crashme.sym
    > Found 1 debug information file
    > Prepared debug information file for upload
    > Uploading completed in 0.208s
    > Uploaded 1 missing debug information file
    > File processing complete:

           OK afd4aa3a-03bc-e8da-b85c-9c31e9319237 (crashme; mips debug companion)
    ```
    ![](assets/breakpad/debug-files-sym-mips.png)

    ```sh
    curl \
        'http://192.168.1.78:9000/api/7/minidump/?sentry_client=breakpad/0.8.9&sentry_key=b134d88980c1686c74a78d4a1c369a31' \
        -F upload_file_minidump=@974109c3-47d7-4d45-3085b59e-6368461c.dmp
    61617094-252b-4a11-a25a-b06bbb151827
    ```
    ![](assets/breakpad/breakpad-crashme-mips-sym.png)

=== "arm"
    ```sh
    dump_syms crashme > crashme.sym
    sentry-cli debug-files upload --wait crashme.sym
    > Found 1 debug information file
    > Prepared debug information file for upload
    > Uploading completed in 0.245s
    > Uploaded 1 missing debug information file
    > File processing complete:

           OK 5b9af048-4e7a-794c-22ac-95b76fe00718 (crashme; arm debug companion)
    ```
    ![](assets/breakpad/debug-files-sym-arm.png)

    ```sh
    curl \
        'http://192.168.1.78:9000/api/7/minidump/?sentry_client=breakpad/0.8.9&sentry_key=b134d88980c1686c74a78d4a1c369a31' \
        -F upload_file_minidump=@ef2f3e3f-86ae-4317-9d879187-f419d6ec.dmp
    0f9c7925-df31-4229-a3fd-23d21fbe0a04
    ```
    ![](assets/breakpad/breakpad-crashme-arm-sym.png)

=== "arm64"
    ```sh
    dump_syms crashme > crashme.sym
    sentry-cli debug-files upload --wait crashme.sym
    > Found 1 debug information file
    > Prepared debug information file for upload
    > Uploading completed in 0.152s
    > Uploaded 1 missing debug information file
    > File processing complete:

           OK d9555d93-2042-a988-00ee-cbee1808c838 (crashme; arm64 debug companion)
    ```
    ![](assets/breakpad/debug-files-sym-arm64.png)

    ```sh
    curl \
        'http://192.168.1.78:9000/api/7/minidump/?sentry_client=breakpad/0.8.9&sentry_key=b134d88980c1686c74a78d4a1c369a31' \
        -F upload_file_minidump=@0798786e-682e-4f79-9b7f1e9f-f15e4657.dmp
    01b75c8b-fbd7-42c2-a133-954106801054
    ```
    ![](assets/breakpad/breakpad-crashme-arm64-sym.png)


## Stack unwinding with ELF binary
=== "mips"
    ```sh
    sentry-cli debug-files upload --wait crashme
    > Found 1 debug information file
    > Prepared debug information file for upload
    > Uploading completed in 0.732s
    > Uploaded 1 missing debug information file
    > File processing complete:

           OK afd4aa3a-03bc-e8da-b85c-9c31e9319237 (crashme; mips executable)
    ```
    ![](assets/breakpad/debug-files-elf-mips.png)

    ```sh
    scp root@192.168.1.91:/tmp/455d87cc-f3d4-4a7f-26da6b83-fc07da78.dmp .

    curl \
        'http://192.168.1.78:9000/api/9/minidump/?sentry_client=breakpad/0.1.5&sentry_key=33ebe98b8197ef62694a73c2b38aa7f0' \
        -F upload_file_minidump=@455d87cc-f3d4-4a7f-26da6b83-fc07da78.dmp
        67de1e96-e7f6-42aa-b999-8d7d15f2f84b
    ```
    ![](assets/breakpad/breakpad-crashme-elf-mips.png)

=== "arm"
    ```sh
    sentry-cli debug-files upload --wait crashme
    > Found 1 debug information file
    > Prepared debug information file for upload
    > Uploading completed in 0.786s
    > Uploaded 1 missing debug information file
    > File processing complete:

           OK 59704203-f7e1-1716-4496-9ebe6ef6db77 (crashme; arm executable)
    ```
    ![](assets/breakpad/debug-files-elf-arm.png)

    ```sh
    scp root@192.168.1.80:/tmp/766a3f95-e8d9-44f5-bcbd53a4-69f442e6.dmp .

    curl \
        'http://192.168.1.62:9000/api/5/minidump/?sentry_client=breakpad/0.1.2&sentry_key=31746ea8c634f7b858299b8712fc547f' \
        -F upload_file_minidump=@766a3f95-e8d9-44f5-bcbd53a4-69f442e6.dmp
    8a06c7bf-d274-4a1c-8240-b0d4c8ac722
    ```
    ![](assets/breakpad/breakpad-crashme-elf-arm.png)


=== "arm64"
    ```sh
    sentry-cli debug-files upload --wait crashme
    > Found 1 debug information file
    > Prepared debug information file for upload
    > Uploading completed in 0.24s
    > Uploaded 1 missing debug information file
    > File processing complete:

           OK ab48bac4-a848-bcef-c226-fc1a4ce454d5 (crashme; arm64 executable)
    ```
    ![](assets/breakpad/debug-files-elf-arm64.png)

    ```sh
    scp root@192.168.1.81:/tmp/b6ff378c-527c-4123-c4557089-b1b96b82.dmp .

    curl \
        'http://192.168.1.62:9000/api/5/minidump/?sentry_client=breakpad/0.1.2&sentry_key=31746ea8c634f7b858299b8712fc547f' \
        -F upload_file_minidump=@b6ff378c-527c-4123-c4557089-b1b96b82.dmp
    23fdda11-b12d-4a67-ad54-9942acf719e1
    ```
    ![](assets/breakpad/breakpad-crashme-elf-arm64.png)


## Stack unwinding with source bundle
=== "mips"
    ```sh
    sentry-cli debug-files upload --wait --include-sources crashme
    > Found 1 debug information file
    > Resolved source code for 1 debug information file
    > Prepared debug information files for upload
    > Uploading completed in 2.339s
    > Uploaded 2 missing debug information files
    > File processing complete:

           OK afd4aa3a-03bc-e8da-b85c-9c31e9319237 (crashme; mips executable)
           OK afd4aa3a-03bc-e8da-b85c-9c31e9319237 (crashme; mips sources)
    ```
    ![](assets/breakpad/debug-files-source-bundle-mips.png)

    ```sh
    scp root@192.168.1.91:/tmp/974109c3-47d7-4d45-3085b59e-6368461c.dmp .

    curl \
        'http://192.168.1.78:9000/api/7/minidump/?sentry_client=breakpad/0.8.9&sentry_key=b134d88980c1686c74a78d4a1c369a31' \
        -F upload_file_minidump=@974109c3-47d7-4d45-3085b59e-6368461c.dmp
    67e64e7f-acdb-4f99-a93a-1679b897eb2b
    ```
    ![](assets/breakpad/breakpad-crashme-source-bundle-mips.png)

=== "arm"
    ```sh
    sentry-cli debug-files upload --include-sources --wait crashme
    > Found 1 debug information file
    > Resolved source code for 1 debug information file
    > Prepared debug information files for upload
    > Uploading completed in 1.486s
    > Uploaded 2 missing debug information files
    > File processing complete:

           OK 5970423b-f7e1-1716-6496-9ebe46f6db77 (crashme; arm executable)
           OK 5970423b-f7e1-1716-6496-9ebe46f6db77 (crashme; arm sources)
    ```
    ![](assets/breakpad/debug-files-source-bundle-arm.png)

    ```sh
    scp root@192.168.1.80:/tmp/ef2f3e3f-86ae-4317-9d879187-f419d6ec.dmp .

    curl \
        'http://192.168.1.78:9000/api/7/minidump/?sentry_client=breakpad/0.8.9&sentry_key=b134d88980c1686c74a78d4a1c369a31' \
        -F upload_file_minidump=@ef2f3e3f-86ae-4317-9d879187-f419d6ec.dmp
    9286126c-cc2a-480f-8a3c-27e927306123
    ```
    ![](assets/breakpad/breakpad-crashme-source-bundle-arm.png)


=== "arm64"
    ```sh
    sentry-cli debug-files upload --include-sources --wait crashme
    > Found 1 debug information file
    > Resolved source code for 1 debug information file
    > Prepared debug information files for upload
    > Uploading completed in 1.73s
    > Uploaded 2 missing debug information files
    > File processing complete:

           OK e510a489-64c6-21cd-b218-59eb5e7d4b40 (crashme; arm64 sources)
           OK e510a489-64c6-21cd-b218-59eb5e7d4b40 (crashme; arm64 executable)
    ```
    ![](assets/breakpad/debug-files-source-bundle-arm64.png)

    ```sh
    scp root@192.168.1.81:/tmp/0798786e-682e-4f79-9b7f1e9f-f15e4657.dmp .

    curl \
        'http://192.168.1.78:9000/api/7/minidump/?sentry_client=breakpad/0.8.9&sentry_key=b134d88980c1686c74a78d4a1c369a31' \
        -F upload_file_minidump=@0798786e-682e-4f79-9b7f1e9f-f15e4657.dmp
    d46af89a-4374-47d8-88df-936a488b119d
    ```
    ![](assets/breakpad/breakpad-crashme-source-bundle-arm64.png)



[breakpad]: https://github.com/google/breakpad
[chromium-breakpad]: https://chromium.googlesource.com/breakpad/breakpad
[guide]: https://chromium.googlesource.com/breakpad/breakpad/+/HEAD/docs/linux_starter_guide.md
[sym]: https://chromium.googlesource.com/breakpad/breakpad/+/master/docs/symbol_files.md
[rm-dif]: https://docs.sentry.io/api/projects/delete-a-specific-projects-debug-information-file/
[rust-minidump]: https://github.com/rust-minidump/rust-minidump
