app=$1
dmp_file=$2

dump_syms ./$app > $app.sym

head -n1 $app.sym

module_id=$(head -n1 "$app".sym | awk '{print $4}')
mkdir -p symbols/$app/$module_id
mv $app.sym symbols/$app/$module_id

minidump_stackwalk $dmp_file ./symbols
