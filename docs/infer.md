---
title: infer
updated: 2024-05-24 21:20:06+0800
created: 2024-05-24 21:20:06+0800
weather: "Qingdao: ☁️   +17°C"
latitude: 36.0649
longitude: 120.3804
categories:
tags:
---


# Build infer
```sh
sudo apt install libstdc++-12-dev linux-libc-dev:i386 gcc-multilib
sudo ln -sf /usr/include/x86_64-linux-gnu/asm /usr/include/asm
git clone https://github.com/facebook/infer
cd infer

mkdir facebook-clang-plugins/clang/src/download
wget -c https://github.com/llvm/llvm-project/releases/download/llvmorg-17.0.1/llvm-project-17.0.1.src.tar.xz -O \
    facebook-clang-plugins/clang/src/download/llvm-project.src.tar.xz

export CLANG_TMP_DIR=/tmp/clang-setup.WIBpUd
JOBS=16 ./build-infer.sh -y clang
[...]
[06:19:38][ 829574] Building clang plugin...
[     16s][ 829574] SUCCESS Building clang plugin
[06:19:54][ 829731] Building clang plugin OCaml interface...
[      1s][ 829731] SUCCESS Building clang plugin OCaml interface
[06:19:55][ 829801] Generating source dependencies...
[      1s][ 829801] SUCCESS Generating source dependencies
[06:19:56][ 829876] Building native(opt) Infer...
[    282s][ 829876] SUCCESS Building native(opt) Infer
[06:24:38][ 837061] Building infer models...
[      6s][ 837061] SUCCESS Building infer models
make[1]: Leaving directory '/home/fdbai/projects/infer'

*** Success! Infer is now built in '/home/fdbai/projects/infer/infer/bin/'.
*** Install infer on your system with `make install`.

*** If you plan to hack on infer, check out CONTRIBUTING.md to setup your dev environment.
sudo make install
```




[infer]: https://github.com/facebook/infer
