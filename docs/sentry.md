---
title: sentry
updated: 2024-05-04 22:05:21+0800
created: 2024-05-04 22:05:21+0800
weather: "Qingdao: ☀️   +16°C"
latitude: 36.0649
longitude: 120.3804
categories:
    - crash
tags:
    - crashpad
    - breakpad
    - native
    - crash
---


# Preparation

```
sudo apt install docker.io
sudo apt install docker-compose-
curl -SL https://github.com/docker/compose/releases/download/v2.24.6/docker-compose-linux-x86_64 -o $HOME/.local/bin/docker-compose
chmod +x $HOME/.local/bin/docker-compose
```



# sentry server

## Install behind proxy
Comment out following two lines in `cron/Dockerfile`:
```
RUN if [ -n "${http_proxy}" ]; then echo "Acquire::http::proxy \"${http_proxy}\";" >> /etc/apt/apt.conf; fi
RUN if [ -n "${https_proxy}" ]; then echo "Acquire::https::proxy \"${https_proxy}\";" >> /etc/apt/apt.conf; fi
```


## Pull and create docker images
```sh
unset http_proxy HTTP_PROXY https_proxy HTTPS_PROXY
VERSION="24.2.0"
git clone https://github.com/getsentry/self-hosted
cd self-hosted
git checkout ${VERSION}
./install.sh --no-report-self-hosted-issues
```


```sh
-----------------------------------------------------------------

You're all done! Run the following command to get Sentry running:

  docker-compose up -d
```


## Configuration
Change `system.url-prefix` to URL to ip.add.re.ss:9000 in `sentry/config.yml`:
```
system.url-prefix: http://192.168.1.89:9000
```


## docker-compose up
This may take several minutes.



# Upload debug information files

```sh
pip install sentry-cli

sentry-cli debug-files find 8184c616-c413-c0cb-2168-5a9dcbbca27e
8184c616-c413-c0cb-2168-5a9dcbbca27e /[...]/projects/sentry/sentry-native/build/sentry_example [elf]

export SENTRY_ORG=sentry
export SENTRY_PROJECT=helloworld
export SENTRY_AUTH_TOKEN=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

sentry-cli debug-files upload --wait build/sentry_example
> Found 1 debug information file
> Prepared debug information file for upload
> Uploading completed in 0.066s
> Uploaded 1 missing debug information file
> File processing complete:

       OK 8184c616-c413-c0cb-2168-5a9dcbbca27e (sentry_example; arm64 library)
```



# sentry-cli

## Install sentry-cli

Ubuntu:
```sh
pip install sentry-cli
```

macOS:
```sh
brew install getsentry/tools/sentry-cli
```


## Configuration

```sh
sentry-cli --url http://192.168.1.78:9000 login -g --auth-token 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
This helps you signing in your sentry-cli with an authentication token.
If you do not yet have a token ready we can bring up a browser for you
to create a token now.

Sentry server: 192.168.1.78
Valid org token

Stored token in /Users/fdbai/.sentryclirc
```

```sh
cat $HOME/.sentryclirc
[auth]
token=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
```


## Upload Source Bundles
```sh
sentry-cli debug-files bundle-sources build/sentry_example
/[...]/projects/sentry/sentry-native/build/sentry_example.src.zip

sentry-cli debug-files upload build/sentry_example.src.zip
> Found 1 debug information file (1 with embedded sources)
> Prepared debug information file for upload
> Uploading completed in 0.158s
> Uploaded 1 missing debug information file
> File upload complete:

  UPLOADED 8184c616-c413-c0cb-2168-5a9dcbbca27e (sentry_example.src.zip; arm64 sources)
```

Or use `--include-sources` to skip creating bundle files:
```sh
sentry-cli debug-files upload --include-sources build/sentry_example
```

![Source Bundle](assets/sentry/source-bundle.png)



## Examples

```sh
sentry-cli debug-files check build/sentry_example
Debug Info File Check
  Type: elf library
  Contained debug identifiers:
    > Debug ID: 8184c616-c413-c0cb-2168-5a9dcbbca27e
      Code ID:  16c6848113c4cbc021685a9dcbbca27e28130539
      Arch:     arm64
  Contained debug information:
    > symtab, debug, unwind
  Usable: yes

sentry-cli releases list
+--------------+-------------------------+------------+--------------+
| Released     | Version                 | New Events | Last Event   |
+--------------+-------------------------+------------+--------------+
| (unreleased) | test-example-release    | 7          | 11 hours ago |
| (unreleased) | frontend@24.2.0+unknown | 1          | -            |
+--------------+-------------------------+------------+--------------+

sentry-cli releases -o sentry -p helloworld list
+--------------+----------------------+------------+--------------+
| Released     | Version              | New Events | Last Event   |
+--------------+----------------------+------------+--------------+
| (unreleased) | test-example-release | 7          | 11 hours ago |
+--------------+----------------------+------------+--------------+

sentry-cli releases info test-example-release
+----------------------+--------------------------------+-------------------------+
| Version              | Date created                   | Last event              |
+----------------------+--------------------------------+-------------------------+
| test-example-release | 2024-03-01 13:39:07.728837 UTC | 2024-03-01 14:06:45 UTC |
+----------------------+--------------------------------+-------------------------+
```


If `$HOME/.sentryclirc` does not exists, export auth token also works:
```sh
export SENTRY_AUTH_TOKEN=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
sentry-cli releases -o sentry -p helloworld list
```



# Backends

## Breakpad

```sh
git clone https://github.com/getsentry/sentry-native --depth 1
git fetch --tags
git checkout 0.7.2
git submodule update --init --recursive
```

=== "mips"
    ```sh
    export PATH=/usr/local/mips/mipsel-buildroot-linux-gnu/bin:$PATH

    cmake -B build -DCMAKE_C_COMPILER=mipsel-linux-gcc \
        -DSENTRY_BACKEND=breakpad \
        -DCMAKE_BUILD_TYPE=RelWithDebInfo
    cmake --build build --parallel
    cmake --install build --prefix /usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr

    sentry-cli debug-files upload --include-sources build/sentry_example
    > Found 1 debug information file
    > Resolved source code for 1 debug information file
    > Prepared debug information files for upload
    > Uploading completed in 0.22s
    > Uploaded 2 missing debug information files
    > File upload complete:

      UPLOADED d383303f-7394-78f6-b11a-73b495bb1193 (sentry_example; arm64 sources)
      UPLOADED d383303f-7394-78f6-b11a-73b495bb1193 (sentry_example; arm64 executable)

    ssh root@192.168.1.91 mkdir /tmp/sentry/
    scp build/sentry_example root@192.168.1.91:/tmp/sentry/
    scp /usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr/lib/libsentry.so root@192.168.1.91:/lib/
    scp /usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr/lib/libcurl.so.4 root@192.168.1.91:/lib/
    ```

=== "arm"
    ```sh
    export PATH=/usr/local/arm/arm-buildroot-linux-gnueabihf/bin/:$PATH
    cmake -B build -DCMAKE_C_COMPILER=arm-linux-gcc \
        -DSENTRY_BACKEND=breakpad \
        -DCMAKE_EXE_LINKER_FLAGS="-Wl,--build-id"
    cmake --build build --parallel
    cmake --install build --prefix /usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr/

    dump_syms build/sentry_example > sentry_example.sym
    sentry-cli debug-files upload --wait sentry_example.sym

    > Found 1 debug information file
    > Prepared debug information file for upload
    > Uploading completed in 0.118s
    > Uploaded 1 missing debug information file
    > File processing complete:

           OK e1748c50-6ec2-4d52-3b9d-0abb22d1e71e (sentry_example; arm debug companion)

    scp /usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr/lib/libsentry.so root@192.168.1.80:/lib/
    ssh root@192.168.1.80 mkdir /tmp/sentry/
    scp build/sentry_example root@192.168.1.80:/tmp/sentry/
    ```

=== "arm64"
    ```sh
    export PATH=/usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/bin:$PATH
    cmake -B build -DCMAKE_C_COMPILER=aarch64-linux-gnu-gcc \
        -DSENTRY_BACKEND=breakpad \
        -DCMAKE_BUILD_TYPE=RelWithDebInfo
    cmake --build build --parallel
    cmake --install build --prefix /usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr

    sentry-cli debug-files upload --include-sources build/sentry_example
    > Found 1 debug information file
    > Resolved source code for 1 debug information file
    > Prepared debug information files for upload
    > Uploading completed in 0.22s
    > Uploaded 2 missing debug information files
    > File upload complete:

      UPLOADED d383303f-7394-78f6-b11a-73b495bb1193 (sentry_example; arm64 sources)
      UPLOADED d383303f-7394-78f6-b11a-73b495bb1193 (sentry_example; arm64 executable)

    scp /usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/lib/libsentry.so root@192.168.1.81:/lib/
    scp /usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/lib/libcurl.so.4 root@192.168.1.81:/lib/
    ssh root@192.168.1.81 mkdir /tmp/sentry/
    scp build/sentry_example root@192.168.1.81:/tmp/sentry/
    ```


### Trigger crash and upload

=== "mips"
    ```sh
    /tmp/sentry # SENTRY_DSN=http://53aa425436d5e7ddec087ec92772f61d@192.168.1.78:9000/8 ./sentry_example log crash
    [sentry] INFO using database path "/tmp/sentry/.sentry-native"
    [sentry] DEBUG starting transport
    [sentry] DEBUG starting background worker thread
    [sentry] DEBUG starting backend
    [sentry] DEBUG processing and pruning old runs
    [sentry] DEBUG background worker thread started
    [sentry] INFO entering breakpad minidump callback
    [sentry] DEBUG merging scope into event
    [sentry] DEBUG trying to read modules from /proc/self/maps
    [sentry] DEBUG read 13 modules from /proc/self/maps
    [sentry] DEBUG adding attachments to envelope
    [sentry] DEBUG sending envelope
    [sentry] DEBUG serializing envelope into buffer
    [sentry] INFO crash has been captured
    Segmentation fault
    ```

    ```sh
    scp -r root@192.168.1.91:/tmp/sentry/.sentry-native .
    sentry-cli send-envelope --raw .sentry-native/9abcb971-f419-42c7-fe84-b1728d40af07.run/9143d88e-eae2-4419-4715-bd8bf1467ad0.envelope
    Envelope from file .sentry-native/9abcb971-f419-42c7-fe84-b1728d40af07.run/9143d88e-eae2-4419-4715-bd8bf1467ad0.envelope dispatched
    ```
    ![](assets/breakpad/sentry-backend-breakpad-mips.png)

=== "arm"
    ```sh
    root@raspberrypi3:/var/volatile/tmp/sentry# SENTRY_DSN=http://33ebe98b8197ef62694a73c2b38aa7f0@192.168.1.78:9000/9 ./sentry_example log crash
    [sentry] INFO using database path "/var/volatile/tmp/sentry/.sentry-native"
    [sentry] DEBUG starting transport
    [sentry] DEBUG starting background worker thread
    [sentry] DEBUG starting backend
    [sentry] DEBUG background worker thread started
    [sentry] DEBUG processing and pruning old runs
    [sentry] INFO entering breakpad minidump callback
    [sentry] DEBUG merging scope into event
    [sentry] DEBUG trying to read modules from /proc/self/maps
    [sentry] DEBUG read 13 modules from /proc/self/maps
    [sentry] DEBUG adding attachments to envelope
    [sentry] DEBUG sending envelope
    [sentry] DEBUG serializing envelope into buffer
    [sentry] INFO crash has been captured
    Segmentation fault
    ```

    ```sh
    scp -r root@192.168.1.80:/tmp/sentry/.sentry-native .
    sentry-cli send-envelope --raw .sentry-native/aea49616-bfbb-4427-f7a4-51fc65f56f7d.run/998337ba-ac12-45ea-4110-c691e26b47df.envelope
    Envelope from file .sentry-native/aea49616-bfbb-4427-f7a4-51fc65f56f7d.run/998337ba-ac12-45ea-4110-c691e26b47df.envelope dispatched
    ```
    ![](assets/breakpad/sentry-backend-breakpad-arm.png)

=== "arm64"
    ```sh
    root@roc-rk3328-cc:/var/volatile/tmp/sentry# SENTRY_DSN=http://53aa425436d5e7ddec087ec92772f61d@192.168.1.78:9000/8 ./sentry_example log crash
    [sentry] INFO using database path "/var/volatile/tmp/sentry/.sentry-native"
    [sentry] DEBUG starting transport
    [sentry] DEBUG starting background worker thread
    [sentry] DEBUG starting backend
    [sentry] DEBUG background worker thread started
    [sentry] DEBUG processing and pruning old runs
    [sentry] INFO entering breakpad minidump callback
    [sentry] DEBUG merging scope into event
    [sentry] DEBUG trying to read modules from /proc/self/maps
    [sentry] DEBUG read 9 modules from /proc/self/maps
    [sentry] DEBUG adding attachments to envelope
    [sentry] DEBUG sending envelope
    [sentry] DEBUG serializing envelope into buffer
    [sentry] INFO crash has been captured
    Segmentation fault
    ```

    ```sh
    scp -r root@192.168.1.81:/tmp/sentry/.sentry-native .
    sentry-cli send-envelope --raw .sentry-native/91e4edf9-ae69-48b2-1699-fdbfec07add4.run/3d50812c-21f4-49b5-3562-185f7ca7d236.envelope
    Envelope from file .sentry-native/91e4edf9-ae69-48b2-1699-fdbfec07add4.run/3d50812c-21f4-49b5-3562-185f7ca7d236.envelope dispatched
    ```
    ![](assets/breakpad/sentry-backend-breakpad-arm64.png)




## Crashpad

```sh
git clone https://github.com/getsentry/sentry-native --depth 1
git fetch --tags
git checkout 0.7.2
git submodule update --init --recursive
```

=== "mips"
    ```sh
    export PATH=/usr/local/mips/mipsel-buildroot-linux-gnu/bin:$PATH

    cmake -B build -DCMAKE_C_COMPILER=mipsel-linux-gcc \
        -DCMAKE_BUILD_TYPE=RelWithDebInfo
    cmake --build build --parallel
    cmake --install build --prefix /usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr

    sentry-cli debug-files upload --include-sources build/sentry_example

    > Found 1 debug information file
    > Resolved source code for 1 debug information file
    > Prepared debug information files for upload
    > Uploading completed in 0.259s
    > Uploaded 2 missing debug information files
    > File upload complete:

      UPLOADED 180633f1-d955-0de5-402d-dc9d76058935 (sentry_example; mips executable)
      UPLOADED 180633f1-d955-0de5-402d-dc9d76058935 (sentry_example; mips sources)

    scp /usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr/bin/crashpad_handler root@192.168.1.91:/bin/
    scp /usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr/lib/libsentry.so root@192.168.1.91:/lib/
    scp /usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr/lib/libcurl.so.4 root@192.168.1.91:/lib/

    ssh root@192.168.1.91 mkdir /tmp/sentry/
    scp build/sentry_example root@192.168.1.91:/tmp/sentry/
    ```

=== "arm"
    ```sh
    export PATH=/usr/local/arm/arm-buildroot-linux-gnueabihf/bin/:$PATH
    cmake -B build -DCMAKE_C_COMPILER=arm-linux-gcc \
        -DCMAKE_EXE_LINKER_FLAGS="-Wl,--build-id"
    cmake --build build --parallel
    cmake --install build --prefix /usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr/

    dump_syms build/sentry_example > sentry_example.sym
    sentry-cli debug-files upload --wait sentry_example.sym

    > Found 1 debug information file
    > Prepared debug information file for upload
    > Uploading completed in 0.075s
    > Uploaded 1 missing debug information file
    > File processing complete:

           OK a2ab32cf-5bc0-5339-96f0-60a97aa2e425 (sentry_example; arm debug companion)

    scp /usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr/bin/crashpad_handler root@192.168.1.80:/bin/
    scp /usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr/lib/libsentry.so root@192.168.1.80:/lib/
    scp /usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr/lib/libcurl.so.4 root@192.168.1.80:/lib/
    ssh root@192.168.1.80 mkdir /tmp/sentry/
    scp build/sentry_example root@192.168.1.80:/tmp/sentry/
    ```

=== "arm64"
    ```sh
    export PATH=/usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/bin:$PATH
    cmake -B build -DCMAKE_C_COMPILER=aarch64-linux-gnu-gcc \
        -DCMAKE_BUILD_TYPE=RelWithDebInfo
    cmake --build build --parallel
    cmake --install build --prefix /usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr

    sentry-cli debug-files upload --include-sources build/sentry_example

    > Found 1 debug information file
    > Resolved source code for 1 debug information file
    > Prepared debug information files for upload
    > Uploading completed in 0.313s
    > Uploaded 2 missing debug information files
    > File upload complete:

      UPLOADED 4722607d-ab06-2945-c180-ec586fc0aed3 (sentry_example; arm64 executable)
      UPLOADED 4722607d-ab06-2945-c180-ec586fc0aed3 (sentry_example; arm64 sources)

    scp /usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/bin/crashpad_handler root@192.168.1.81:/bin/
    scp /usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/lib/libsentry.so root@192.168.1.81:/lib/
    scp /usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr/lib/libcurl.so.4 root@192.168.1.81:/lib/
    ssh root@192.168.1.81 mkdir /tmp/sentry/
    scp build/sentry_example root@192.168.1.81:/tmp/sentry/
    ```


### Trigger crash and upload

=== "mips"
    ```sh
    /tmp/sentry # SENTRY_DSN=http://33ebe98b8197ef62694a73c2b38aa7f0@192.168.1.78:9000/9 ./sentry_example log crash
    [sentry] INFO using database path "/var/tmp/sentry/.sentry-native"
    [sentry] DEBUG starting transport
    [sentry] DEBUG starting background worker thread
    [sentry] DEBUG starting backend
    [sentry] DEBUG starting crashpad backend with handler "/bin/crashpad_handler"
    [sentry] DEBUG background worker thread started
    [sentry] DEBUG using minidump URL "http://192.168.1.78:9000/api/9/minidump/?sentry_client=sentry.native/0.7.2&sentry_key=33ebe98b8197ef62694a73c2b38aa7f0"
    [sentry] INFO started crashpad client handler
    [sentry] DEBUG processing and pruning old runs
    [sentry] INFO flushing session and queue before crashpad handler
    [sentry] INFO handing control over to crashpad
    [3010:3010:20240518,123327.222966:ERROR process_memory_linux.cc:50] pread64: Input/output error (5)
    [3010:3010:20240518,123327.227446:WARNING process_reader_linux.cc:146] no stack mapping
    [3010:3010:20240518,123327.229047:ERROR file_io_posix.cc:145] open /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq: No such file or directory (2)
    [3010:3010:20240518,123327.229417:ERROR file_io_posix.cc:145] open /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq: No such file or directory (2)
    Segmentation fault
    ```

    ![](assets/crashpad/sentry-backend-crashpad-mips.png)

=== "arm"
    ```sh
    root@raspberrypi3:/var/volatile/tmp/sentry# SENTRY_DSN=http://33ebe98b8197ef62694a73c2b38aa7f0@192.168.1.78:9000/9 ./sentry_example log crash
    [sentry] INFO using database path "/var/volatile/tmp/sentry/.sentry-native"
    [sentry] DEBUG starting transport
    [sentry] DEBUG starting background worker thread
    [sentry] DEBUG starting backend
    [sentry] DEBUG starting crashpad backend with handler "/bin/crashpad_handler"
    [sentry] DEBUG background worker thread started
    [sentry] DEBUG using minidump URL "http://192.168.1.78:9000/api/9/minidump/?sentry_client=sentry.native/0.7.2&sentry_key=33ebe98b8197ef62694a73c2b38aa7f0"
    [sentry] INFO started crashpad client handler
    [sentry] DEBUG processing and pruning old runs
    [sentry] INFO flushing session and queue before crashpad handler
    [sentry] INFO handing control over to crashpad
    Segmentation fault
    ```

    ![](assets/crashpad/sentry-backend-crashpad-arm.png)

=== "arm64"
    ```sh
    root@roc-rk3328-cc:/var/volatile/tmp/sentry# SENTRY_DSN=http://33ebe98b8197ef62694a73c2b38aa7f0@192.168.1.78:9000/9 ./sentry_example log crash
    [sentry] INFO using database path "/var/volatile/tmp/sentry/.sentry-native"
    [sentry] DEBUG starting transport
    [sentry] DEBUG starting background worker thread
    [sentry] DEBUG starting backend
    [sentry] DEBUG starting crashpad backend with handler "/bin/crashpad_handler"
    [sentry] DEBUG background worker thread started
    [sentry] DEBUG using minidump URL "http://192.168.1.78:9000/api/9/minidump/?sentry_client=sentry.native/0.7.2&sentry_key=33ebe98b8197ef62694a73c2b38aa7f0"
    [sentry] INFO started crashpad client handler
    [sentry] DEBUG processing and pruning old runs
    [sentry] INFO flushing session and queue before crashpad handler
    [sentry] INFO handing control over to crashpad
    Segmentation fault
    ```

    ![](assets/crashpad/sentry-backend-crashpad-arm64.png)



# Relay Server

```sh
docker run --rm -it                  \
  -v $(pwd)/config/:/work/.relay/:z  \
  --entrypoint bash                  \
  getsentry/relay:24.3.0             \
  -c 'chown -R relay:relay /work/.relay'
```

```sh
# Generate the configuration
docker run --rm -it                  \
  -v $(pwd)/config/:/work/.relay/:z  \
  getsentry/relay:24.3.0             \
  config init
Initializing relay in /work/.relay
✔ Do you want to create a new config? · Yes, create default config
Generated new credentials
  relay id: 2444abd0-7779-4887-9dce-444c56f8d141
  public key: hIoJVkTE8_GDBC6ORmPTxuF-nDAQ7Qi2OPChVZiRJhQ
All done!
```


cat config/config.yml
```yaml
relay:
  mode: managed
  upstream: http://192.168.1.62:9000/
  host: 0.0.0.0
  port: 3000
logging:
  level: TRACE
```

cat docker-compose.yml
```yaml
services:
  relay:
    image: getsentry/relay:24.3.0
    network_mode: host
    ports:
      - 3000:3000
    volumes:
      - type: bind
        read_only: true
        source: ./config
        target: /work/.relay
```


```sh
docker-compose up
[+] Running 2/0
 ✔ Container relay-relay-1                                          Created                                                              0.1s
 ! relay Published ports are discarded when using host network mode                                                                      0.0s
Attaching to relay-relay-1
relay-relay-1  | 2024-03-30T13:50:22.162697Z  INFO relay::setup: launching relay from config folder /work/.relay
relay-relay-1  | 2024-03-30T13:50:22.162724Z  INFO relay::setup:   relay mode: managed
relay-relay-1  | 2024-03-30T13:50:22.162727Z  INFO relay::setup:   relay id: 2444abd0-7779-4887-9dce-444c56f8d141
relay-relay-1  | 2024-03-30T13:50:22.162732Z  INFO relay::setup:   public key: hIoJVkTE8_GDBC6ORmPTxuF-nDAQ7Qi2OPChVZiRJhQ
relay-relay-1  | 2024-03-30T13:50:22.162735Z  INFO relay::setup:   log level: trace
relay-relay-1  | 2024-03-30T13:50:22.162744Z  INFO relay_server: relay server starting
relay-relay-1  | 2024-03-30T13:50:22.172599Z  INFO relay_server::services::outcome: Configured to emit outcomes as client reports
relay-relay-1  | 2024-03-30T13:50:22.172676Z  INFO relay_server::services::global_config: global config service starting
relay-relay-1  | 2024-03-30T13:50:22.172717Z  INFO relay_server::services::global_config: requesting global config from upstream
relay-relay-1  | 2024-03-30T13:50:22.172745Z  INFO relay_server::services::processor: starting 8 envelope processing workers
relay-relay-1  | 2024-03-30T13:50:22.172636Z  INFO relay_server::services::upstream: registering with upstream descriptor=http://192.168.1.62:9000/
relay-relay-1  | 2024-03-30T13:50:22.172833Z  INFO relay_metrics::router: metrics router started
relay-relay-1  | 2024-03-30T13:50:22.172923Z  INFO relay_server::services::relays: key cache started
relay-relay-1  | 2024-03-30T13:50:22.173083Z  INFO relay_server::services::outcome: OutcomeProducer started.
relay-relay-1  | 2024-03-30T13:50:22.173096Z  INFO relay_server::services::project_cache: project cache started
relay-relay-1  | 2024-03-30T13:50:22.173125Z  INFO relay_server::services::outcome_aggregator: outcome aggregator started
relay-relay-1  | 2024-03-30T13:50:22.173187Z  INFO relay_server::services::project_cache: waiting for global config
relay-relay-1  | 2024-03-30T13:50:22.173281Z  INFO relay_server::services::project_upstream: project upstream cache started
relay-relay-1  | 2024-03-30T13:50:22.173280Z  INFO relay_server::services::project_local: project local cache started
relay-relay-1  | 2024-03-30T13:50:22.173412Z  INFO relay_server::services::server: spawning http server
relay-relay-1  | 2024-03-30T13:50:22.173419Z  INFO relay_server::services::server:   listening on http://0.0.0.0:3000/
relay-relay-1  | 2024-03-30T13:50:22.173354Z DEBUG relay_server::utils::garbage: Start garbage collection thread
relay-relay-1  | 2024-03-30T13:50:22.546831Z DEBUG relay_server::services::upstream: got register challenge token="eyJ0aW1lc3RhbXAiOjE3MTE4MDY2MjIsInJlbGF5X2lkIjoiMjQ0NGFiZDAtNzc3OS00ODg3LTlkY2UtNDQ0YzU2ZjhkMTQxIiwicHVibGljX2tleSI6ImhJb0pWa1RFOF9HREJDNk9SbVBUeHVGLW5EQVE3UWkyT1BDaFZaaVJKaFEiLCJyYW5kIjoiMDB3ZnJ5cHZJemJ2bXJ5ZjZWM2VzVjVHdkF3a1hCV2hQWnN6Z29NVnlsWWZUZkdIaVFSWkt6bkUxR0tfd2locmtiOXBrUUtyaDM0LVc4ZFBBSm00M1EifQ:_F7AGcZZtp18cskC-rjPY86Q9vzzGUzx7cy86a_KUw413u5fQILLN-XucezwAGh2ETcwgE1OH8HEqlKZj-1qiA"
relay-relay-1  | 2024-03-30T13:50:22.546882Z DEBUG relay_server::services::upstream: sending register challenge response
relay-relay-1  | 2024-03-30T13:50:23.430135Z  INFO relay_server::services::upstream: relay successfully registered with upstream
relay-relay-1  | 2024-03-30T13:50:23.747339Z  INFO relay_server::services::global_config: received global config from upstream
```


Copy the public key of relay server and register it to sentry server:
```sh
docker run --rm -it                \
  -v $(pwd)/config/:/work/.relay/  \
  getsentry/relay:24.3.0           \
  credentials show
Credentials:
  relay id: 2444abd0-7779-4887-9dce-444c56f8d141
  public key: hIoJVkTE8_GDBC6ORmPTxuF-nDAQ7Qi2OPChVZiRJhQ
```

![Relay register](assets/sentry/relay-register.png)

![Relay registered](assets/sentry/relay-registered.png)


## Report crash event to relay server
```sh
root@roc-rk3328-cc:/usr/bin# SENTRY_DSN=http://d876ef1d51a5b762eaaffa49cbc9b526@192.168.1.78:3000/2 ./sentry_example log abort
[sentry] INFO using database path "/usr/bin/.sentry-native"
[sentry] DEBUG starting transport
[sentry] DEBUG starting background worker thread
[sentry] DEBUG starting backend
[sentry] DEBUG starting crashpad backend with handler "/usr/bin/crashpad_handler"
[sentry] DEBUG background worker thread started
[sentry] DEBUG using minidump URL "http://192.168.1.78:3000/api/2/minidump/?sentry_client=sentry.native/0.7.0&sentry_key=d876ef1d51a5b762eaaffa49cbc9b526"
[sentry] INFO started crashpad client handler
[sentry] DEBUG processing and pruning old runs
[sentry] INFO flushing session and queue before crashpad handler
[sentry] INFO handing control over to crashpad
Aborted
```



# Troubleshooting


## CSRF Verification Failed

![CSRF Verification Failed](assets/sentry/csrf-verification-failed.png)

```
CSRF Verification Failed
A required security token was not found or was invalid
```

```
sentry-self-hosted-web-1                                           | 02:28:08 [WARNING] django.security.csrf: Forbidden (Origin checking failed - http://192.168.1.89:9000 does not match any trusted origins.): /auth/login/sentry/ (status_code=403 request=<WSGIRequest: POST '/auth/login/sentry/'>)
```


**Answer**
sentry/config.yml
```
system.url-prefix: http://192.168.1.89:9000
```


## No Organization Access

![No Organization Access](assets/sentry/no-organization-access.png)

All the users create with command line has this issue, and are not listed in
webUI:
```
docker compose run --rm web createuser --email fudongbai@gmail.com --password 123456 --org-id sentry --superuser
```
I create superuser with above command because user create was skipped in the
installation process:
```sh
./install.sh --skip-user-creation --no-report-self-hosted-issues
```


**Answer**
I think this is a bug, but didn't find a solution, so I use this command for sentry install:
```sh
./install.sh --no-report-self-hosted-issues
```


## A required debug information file was missing

![debug information](assets/sentry/debug-info-missing.png)

**Answer**
Upload debug info before report crashes to sentry.


## authentication encountered error error
```sh
docker-compose up
[...]
relay-relay-1  | 2024-03-29T21:39:51.351818Z  INFO relay_server::services::server: spawning http server
relay-relay-1  | 2024-03-29T21:39:51.351826Z  INFO relay_server::services::server:   listening on http://0.0.0.0:3000/
relay-relay-1  | 2024-03-29T21:39:51.351751Z DEBUG relay_server::utils::garbage: Start garbage collection thread
relay-relay-1  | 2024-03-29T21:39:54.352990Z ERROR relay_server::services::upstream: authentication encountered error error=could not send request to upstream error.sources=[error sending request for url (http://192.168.1.62:9000/api/0/relays/register/challenge/): error trying to connect: operation timed out, error trying to connect: operation timed out, operation timed out]
relay-relay-1  | 2024-03-29T21:39:54.353224Z DEBUG relay_server::services::upstream: scheduling authentication retry in 0 seconds
relay-relay-1  | 2024-03-29T21:39:54.354327Z  INFO relay_server::services::upstream: registering with upstream descriptor=http://192.168.1.62:9000/
relay-relay-1  | 2024-03-29T21:39:57.355775Z ERROR relay_server::services::upstream: authentication encountered error error=could not send request to upstream error.sources=[error sending request for url (http://192.168.1.62:9000/api/0/relays/register/challenge/): error trying to connect: operation timed out, error trying to connect: operation timed out, operation timed out]
```

**Answer**
Add `network_mode: host` to docker-compose.yml.



[self-hosted]: https://develop.sentry.dev/self-hosted
[sentry-native]: https://github.com/getsentry/sentry-native
[usage]: https://docs.sentry.io/platforms/native/usage
[docker-compose]: https://docs.docker.com/compose/install/standalone
[geolocation]: https://develop.sentry.dev/self-hosted/geolocation
[getting-started]: http://192.168.1.89:9000/sentry/helloworld/getting-started/native
[doc-native]: https://docs.sentry.io/platforms/native
[backends]: https://docs.sentry.io/platforms/native/configuration/backends
[releases]: https://docs.sentry.io/product/cli/releases
[cli-config]: https://docs.sentry.io/product/cli/configuration
[dif]: https://docs.sentry.io/product/cli/dif
[crashpad]: https://docs.sentry.io/platforms/native/configuration/backends/crashpad
[size-limits]: https://docs.sentry.io/platforms/native/guides/minidumps/#size-limits
[3969]: https://forum.sentry.io/t/no-organization-access/3969/5
[708]: https://github.com/getsentry/self-hosted/issues/708#issuecomment-713018113
[email]: https://develop.sentry.dev/self-hosted/email
[gitlab]: https://docs.sentry.io/product/integrations/source-code-mgmt/gitlab
[relay]: https://docs.sentry.io/product/relay/getting-started
[relay-option]: https://docs.sentry.io/product/relay/options
[dart]: https://github.com/getsentry/sentry-dart
[sentry]: https://github.com/getsentry/sentry
[envelope]: https://develop.sentry.dev/sdk/envelopes
