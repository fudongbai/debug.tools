---
title: pahole
updated: 2024-06-16 15:35:37+0800
created: 2024-06-16 15:35:37+0800
weather: "Qingdao: ☀️   +25°C"
latitude: 36.0649
longitude: 120.3804
categories:
tags:
---

# Install
```sh
sudo apt install pahole
```


[pahole]: https://git.kernel.org/pub/scm/devel/pahole/pahole.git
