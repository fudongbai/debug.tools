---
title: valgrind
updated: 2024-05-05 07:59:47+0800
created: 2024-05-05 07:59:47+0800
weather: "Qingdao: 🌦   +12°C"
latitude: 36.0649
longitude: 120.3804
categories:
tags:
---

##

# Fetch code
```sh
git clone https://sourceware.org/git/valgrind.git
git checkout VALGRIND_3_22_0
./autogen.sh
```

# Build
=== "mips"
    ```sh
    export PATH=/usr/local/mips/mipsel-buildroot-linux-gnu/bin:$PATH
    mkdir build-mips
    pushd build-mips
    ../configure --host=mipsel-linux \
        --prefix=/usr/local/mips/mipsel-buildroot-linux-gnu/mipsel-buildroot-linux-gnu/sysroot/usr
    make -j$(nproc) && make install
    popd
    ```

    For mips, the following patch is required:
    ```patch
    --8<-- "docs/assets/valgrind/0001-Disable-the-valgrind-helpers-which-use-MIPS-floating.patch"
    ```

=== "arm"
    ```sh
    export PATH=/usr/local/arm/arm-buildroot-linux-gnueabihf/bin/:$PATH
    mkdir build-arm
    pushd build-arm
    ../configure --host=arm-linux \
        --prefix=/usr/local/arm/arm-buildroot-linux-gnueabihf/arm-buildroot-linux-gnueabihf/sysroot/usr
    make -j$(nproc) && make install
    popd
    ```

=== "arm64"
    ```sh
    export PATH=/usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/bin:$PATH
    mkdir build-arm64
    pushd build-arm64
    ../configure --host=aarch64-linux-gnu \
        --prefix=/usr/local/aarch64/gcc-linaro-11.3.1-2022.06-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/libc/usr
    make -j$(nproc) && make install
    popd
    ```







[valgrind]: https://valgrind.org
[mips-patch]: https://git.openwrt.org/?p=openwrt/openwrt.git;a=blob_plain;f=package/devel/valgrind/patches/130-mips_fix_soft_float.patch
