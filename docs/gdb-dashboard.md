---
title: gdb-dashboard
updated: 2024-05-05 10:34:28+0800
created: 2024-05-05 10:34:28+0800
weather: "Qingdao: 🌦   +12°C"
latitude: 36.0649
longitude: 120.3804
categories:
tags:
---

# Install
```sh
wget -P ~ https://github.com/cyrus-and/gdb-dashboard/raw/master/.gdbinit
pip install pygments
```

[gdb-dashboard]: https://github.com/cyrus-and/gdb-dashboard
[wiki]: https://github.com/cyrus-and/gdb-dashboard/wiki
